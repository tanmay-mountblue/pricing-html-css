function validation() {
  let username = document.getElementById("username").value;
  let email = document.getElementById("email").value;
  let password = document.getElementById("password").value;
  let confirm_password = document.getElementById("confirm_password").value;

  if (!username) {
    document.getElementById("username").style.border = "1px solid red";
  } else if (!email) {
    document.getElementById("email").style.border = "1px solid red";
  } else if (!password) {
    document.getElementById("password").style.border = "1px solid red";
  } else if (!confirm_password) {
    document.getElementById("confirm_password").style.border = "1px solid red";
  } else {
    if (password === confirm_password) {
      location.href = "pricing.html";
    } else {
      alert("Passwords Do not Match!");
    }
  }
}

function changeBorder(id) {
  console.log(id);
  document.getElementById(id).style.border = "0";
}
